package org.ssldev.playcounter.utils;

import java.io.File;

public class TextUtils {
	
	/**
	 * transforms absolute path into file name
	 * @param fullPath to transform
	 * @return file name of path
	 */
	public static String removePath(String fullPath) {
		int index = fullPath.lastIndexOf(File.separatorChar);
		String name = fullPath.substring(index+1);
		return name;
	}

}
