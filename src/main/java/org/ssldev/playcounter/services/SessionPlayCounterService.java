package org.ssldev.playcounter.services;

import static org.ssldev.playcounter.utils.TextUtils.removePath;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import org.ssldev.api.SeratoSessionFile;
import org.ssldev.api.chunks.AdatC;
import org.ssldev.api.messages.SeratoSessionsLoadCompleteMessge;
import org.ssldev.core.mgmt.EventHub;
import org.ssldev.core.services.Service;
import org.ssldev.core.utils.Logger;
import org.ssldev.core.utils.StopWatch;
import org.ssldev.playcounter.messages.Mp3PlayCounts;
import org.ssldev.playcounter.messages.PlayCounterAppSettings;

/**
 * counts the number of times mp3 files appear in serato session files
 * </p>
 * Consumes: 
 * <ul>
 * <li>{@link SeratoSessionsLoadCompleteMessge}</li>
 * <li>{@link PlayCounterAppSettings}</li>
 * </ul>
 * Produces:
 * <ul>
 * <li>{@link Mp3PlayCounts}</li>
 * </ul>
 */
public class SessionPlayCounterService extends Service{

	/**
	 * map of absolute path of an mp3 and the number of times it was played in a session file
	 */
	private ConcurrentMap<String, AtomicLong> countsPerSong = new ConcurrentHashMap<>();
	
	/**
	 * previously counted songs.  used to keep track of how many tracks were counted per message
	 */
	private long prevCount = 0;
	
	public SessionPlayCounterService(EventHub hub) {
		super(hub);
	}
	
	@Override
	public void init() {
		hub.register(SeratoSessionsLoadCompleteMessge.class, msg -> countTracksInSessions(msg.loadedSessionFiles));
	}
	
	
	private void countTracksInSessions(Map<String, SeratoSessionFile> sessions) {
		Logger.info(this, "start counting tracks in ["+sessions.size()+"] sessions");
		
		StopWatch procTime = new StopWatch().start();
		
		sessions.values().stream()
		.flatMap(sess -> sess.adatsInSessionOrderedByStartTime.stream())
		.forEach(this::count);
		
		Logger.info(this, "finished tabulating counts for ["+(countsPerSong.size()- prevCount)+"] tracks in ["+procTime.stop()+"] time");
		
		prevCount = countsPerSong.size();
		
		AtomicLong max = new AtomicLong();
		AtomicReference<String> maxP = new AtomicReference<>("");
		countsPerSong.forEach( (p, c) ->{
			if(c.get() > max.get()) {
				max.set( c.get());
				maxP.set( p);
			}
		});
		
		Logger.info(this, "biggest count = ["+max.get()+"]["+maxP.get()+"]");
		
		hub.add(new Mp3PlayCounts(new HashMap<>(countsPerSong)));
	}
	

	private void count(AdatC a) {
		countsPerSong.computeIfAbsent(removePath(a.fullPath), s -> new AtomicLong(0))
					 .getAndIncrement();
	}
	
}
