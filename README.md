Serato-Play-Counter
===================

This tool will: 
1. scan serato historical session files
2. tabulate the number of times each track was played
3. attempt to locate all mp3s matching each track played.
4. finally, overwrite the id3 field serato uses to store play counts ('TXXX:SERATO_PLAYCOUNT') 
with the tabulated value.

Disclaimer:
-----------
- i've only tested this tool against my own library, Serato DJ Pro 2.2.0. and 
OSX Mojave.  While my experience has been positive yours may not. No warranties
are given or implied.  use at your own risk!

PREREQS:
--------
- needs at least JRE 8 to run (tested with JRE build 1.8.0_201-b09)


HOW-TO:
-------

before starting:
* BACKUP all music library & serato files!  Writing ID3 tags is a destructive
edit and may potentially corrupt one or all files processed *



to run the tool:
1. shutdown Serato
1. download the jar that contains the tool: 
    - https://gitlab.com/eladmaz/serato-play-counter/blob/master/SeratoPlayCounter-1.0-SNAPSHOT-jar-with-dependencies.jar
1. open terminal (OSX)
1. run the tool with '-s' followed by the path to your serato session files, and 
'-m' followed by the path to the parent dir that contains all mp3s.
1. start Serato and rescan id3 tags for all files

Example)
```
java -jar SeratoPlayCounter-1.0-SNAPSHOT-jar-with-dependencies.jar -s /Users/elad/Music/_Serato_/History/Sessions -m /Users/elad/Music/
```
--------


*The tool is made possible by the following API:*
https://gitlab.com/eladmaz/SSL-API


*More info on other serato tools/projects:*
http://projects.ssldev.org


