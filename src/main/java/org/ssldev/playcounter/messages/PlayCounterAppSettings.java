package org.ssldev.playcounter.messages;

import static java.util.Objects.requireNonNull;

import java.io.File;

import org.ssldev.core.messages.Message;
import org.ssldev.core.utils.Validate;

/**
 * app settings for counting mp3 plays
 */
public class PlayCounterAppSettings extends Message {

	public final String mp3ParentDirAbPath;
	public final File mp3ParentDirFile;

	public PlayCounterAppSettings(String parentDirAbsolutePath) {
		requireNonNull(parentDirAbsolutePath);
		
		mp3ParentDirFile = new File (parentDirAbsolutePath);
		Validate.isTrue(mp3ParentDirFile.exists(), parentDirAbsolutePath + ": directory does not exist");
		Validate.isTrue(mp3ParentDirFile.isDirectory(), parentDirAbsolutePath + ": is not a directory");
		
		this.mp3ParentDirAbPath = parentDirAbsolutePath;
	}
}
