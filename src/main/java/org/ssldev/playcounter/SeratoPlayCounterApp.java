package org.ssldev.playcounter;

import static java.lang.System.lineSeparator;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.ssldev.api.messages.LocateValidSeratoSessionFilesMessage;
import org.ssldev.api.services.HistoricalSessionsLocatorService;
import org.ssldev.api.services.SeratoSessionsLoaderService;
import org.ssldev.api.services.SessionBytesPublisherService;
import org.ssldev.api.services.SslSessionFileUnmarshalService;
import org.ssldev.core.mgmt.AsyncEventHub;
import org.ssldev.core.services.FileReaderService;
import org.ssldev.core.services.FileWriterService;
import org.ssldev.core.services.SystemInfoValidatorService;
import org.ssldev.core.utils.Logger;
import org.ssldev.core.utils.SysInfo;
import org.ssldev.playcounter.messages.PlayCountTagsWritingFinished;
import org.ssldev.playcounter.messages.PlayCounterAppSettings;
import org.ssldev.playcounter.services.Id3TagPlayCountWriteService;
import org.ssldev.playcounter.services.Mp3LocatorService;
import org.ssldev.playcounter.services.SessionPlayCounterService;

/**
 * calculates and writes mp3 play counts into files
 */
public class SeratoPlayCounterApp 
{
	public static String APP_NAME = "Serato Play Counter App";
	public static final String VERSION = "v0.1";
	
	private AsyncEventHub hub;
//	private java.util.logging.Logger[] pins;
	private static String sessPath;
	private static String mp3Path;
	
    public static void main( String[] args )
    {
    	//get MP3 parent dir from args
    	//get Session files parent dir from args
    	if(!verifyArgs(Arrays.asList(args))) {
    		return;
    	}
    	
    	SeratoPlayCounterApp app = new SeratoPlayCounterApp(new AsyncEventHub(SysInfo.getNumAvailableThreads()));
        
		// register services..
		app.init();
		// start
		app.start();

		app.hub.add(loadSessionsFrom(sessPath));
		app.hub.add(locateMp3sToTagIn(mp3Path));
    }
    

	public SeratoPlayCounterApp(AsyncEventHub hub) {
    	this.hub =  hub;
    	
    	Runtime.getRuntime().addShutdownHook(new Thread(()-> shutdown()));  
	}
    
	private void init() {
		Logger.enableDebug(false);
		Logger.enableTrace(false);
		Logger.setShowTime(true);

//		pins = new java.util.logging.Logger[]{ java.util.logging.Logger.getLogger("org.jaudiotagger") };
//		for (java.util.logging.Logger l : pins)
//			l.setLevel(Level.WARNING);
		
		
		// register all app services
		registerStatsServices(hub);
		
		// shutdown the app once we're done tagging all tracks with playcount info
		hub.register(PlayCountTagsWritingFinished.class, m -> this.shutdown());
//		hub.register(DbPlayCountWritingFinished.class, m -> this.shutdown());
	}
	
	@SuppressWarnings("unused")
	private void registerStatsServices(AsyncEventHub hub) {
		Logger.info(this, "Registering all "+APP_NAME+" services:");
		
		/* CORE services */
		SystemInfoValidatorService sysInfoService 		 = new SystemInfoValidatorService(hub);
		FileReaderService fileReadService 				 = new FileReaderService(hub);
		FileWriterService writerService 				 = new FileWriterService(hub);
		
		/* PLAY COUNTER services */
		SessionPlayCounterService totalPlaysCounter      = new SessionPlayCounterService(hub);
		Mp3LocatorService mp3Locator 					 = new Mp3LocatorService(hub);
		Id3TagPlayCountWriteService id3Writer         	 = new Id3TagPlayCountWriteService(hub);
//		DbPlayCountWriteService dbPlayCountWriter	     = new DbPlayCountWriteService(hub);
		
		/* SERATO API services (in lieu of starting API directly) */
		HistoricalSessionsLocatorService sessionFilesLocator     = new HistoricalSessionsLocatorService(hub);
		SeratoSessionsLoaderService sessionFilesLoader           = new SeratoSessionsLoaderService(hub);
		SessionBytesPublisherService adatsConsumedPubService 	 = new SessionBytesPublisherService(hub);
		SslSessionFileUnmarshalService sessionFileslUnmarshalService 
														         = new SslSessionFileUnmarshalService(hub);
	}
	
	private static LocateValidSeratoSessionFilesMessage loadSessionsFrom(String path) {
		
		LocateValidSeratoSessionFilesMessage msg = new LocateValidSeratoSessionFilesMessage(new File(path));
		msg.maxAgeInMonths = Integer.MAX_VALUE;
		msg.maxFileSizeInBytes = Integer.MAX_VALUE;
		msg.minFileSizeInBytes =  0;
		msg.maxNumFiles = Integer.MAX_VALUE;
		
		return msg;
	}
	
	private static PlayCounterAppSettings locateMp3sToTagIn(String path) {
		return new PlayCounterAppSettings(path);
	}

	private void start() {
		hub.init();
		hub.start();
	}
	
	private void shutdown() {
		hub.shutdown();
	}
	
	
	private static boolean verifyArgs(List<String> args) {
		if(args.size() != 4 || !args.contains("-s") || !args.contains("-m")) {
			System.err.println("invalid arguments. size "+args.size()+" " +lineSeparator()+help());
			
			
			return false;
		}

		for(int i = 0; i<args.size() ; i++) {
			System.out.println(i + ": "+args.get(i));
		}
		
		sessPath = args.get(args.indexOf("-s") + 1);
		mp3Path = args.get(args.indexOf("-m") + 1);
		
		File sessPathFile = new File(sessPath);
		File mp3PathFile = new File(mp3Path);
		
		if(!sessPathFile.exists() || !sessPathFile.isDirectory()) {
			System.err.println("sessions dir ["+sessPath+"] either does not exist or is not a directory.  "
					+lineSeparator()+help());
			return false;
		}
		if(!mp3PathFile.exists() || !mp3PathFile.isDirectory()) {
			System.err.println("mp3 dir ["+mp3Path+"] either does not exist or is not a directory.  "
					+lineSeparator()+help());
			return false;
		}
		
		Logger.info(SeratoPlayCounterApp.class, "given session path is ["+sessPath+"]");
		Logger.info(SeratoPlayCounterApp.class, "given session path is ["+mp3Path+"]");

		return true;
	}

	private static String help() {
		return "usage: java -jar [jar name] -s [dir location of session files] -m [dir location of mp3 files]";
	}
}
