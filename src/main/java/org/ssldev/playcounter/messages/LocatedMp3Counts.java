package org.ssldev.playcounter.messages;

import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;

import org.ssldev.core.messages.Message;

/**
 * all mp3s located and their associated play counts (unmodifiable)
 */
public class LocatedMp3Counts extends Message {

	/**
	 * path of all located mp3s and their associated play counts (unmodifiable)
	 */
	public final Map<Path, Long> countsByMp3Path;

	public LocatedMp3Counts(Map<Path, Long> countsByMp3Path) {
		this.countsByMp3Path = Collections.unmodifiableMap(countsByMp3Path);
	}

}
