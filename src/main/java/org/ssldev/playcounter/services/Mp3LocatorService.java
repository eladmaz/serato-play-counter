package org.ssldev.playcounter.services;

import static java.util.stream.Collectors.groupingBy;
import static org.ssldev.playcounter.utils.TextUtils.removePath;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.ssldev.core.mgmt.EventHub;
import org.ssldev.core.services.Service;
import org.ssldev.core.utils.Logger;
import org.ssldev.core.utils.StopWatch;
import org.ssldev.playcounter.messages.LocatedMp3Counts;
import org.ssldev.playcounter.messages.Mp3PlayCounts;
import org.ssldev.playcounter.messages.PlayCounterAppSettings;

/**
 * recursively searches for MP3 files within a given parent directory
 * </p>
 * Consumes: 
 * <ul>
 * <li>{@link Mp3PlayCounts}</li>
 * </ul>
 * Produces:
 * <ul>
 * <li>{@link LocatedMp3Counts}</li>
 * </ul>
 */
public class Mp3LocatorService extends Service {

	/**
	 * paths of all mp3s in system under given directory, by name 
	 */
	private Map<String, List<Path>> mp3sByName = Collections.emptyMap();

	/**
	 * top-most directory containing mp3 files
	 */
	private String mp3ParentDirAbsPath = null;

	
	public Mp3LocatorService(EventHub hub) {
		super(hub);
	}
	
	@Override
	public void init() {
		hub.register(Mp3PlayCounts.class, this::locateMp3s);
		hub.register(PlayCounterAppSettings.class, msg -> mp3ParentDirAbsPath = msg.mp3ParentDirAbPath);
	}

	
	private void locateMp3s(Mp3PlayCounts msg) {
		
		if(null == mp3ParentDirAbsPath) {
			Logger.warn(this, "mp3 parent directory was not set.  aborting mp3 location search");
			return;
		}
		
		StopWatch locateTimer = new StopWatch().start();
		
		// may need to map out all mp3s 
		if(mp3sByName.isEmpty()) {
			
			StopWatch mp3LookupTimer = new StopWatch().start();
			
			Logger.info(this, "creating mp3 lookup of all mp3s in ["+mp3ParentDirAbsPath+"]");
			
			mp3sByName = locateAllMp3s(mp3ParentDirAbsPath);
			
			Logger.info(this, "found a total of ["+mp3sByName.size()+"] mp3s (took "+mp3LookupTimer.stop()+")");
		}

		// map of located mp3s and counts
		Map<Path, Long> countsOfLocatedMp3s = new HashMap<>();
		
		// for each mp3 found in session files...
		for(Map.Entry<String,AtomicLong> entry : msg.playCountsPerMp3Name.entrySet()) {
			String name = entry.getKey();
			long count = entry.getValue().get();
			
			//.. filter it out if its not in the current system
			if(mp3sByName.containsKey(name)) {
				
				//.. add current mp3 location and the number of times it was counted in the session files
				mp3sByName.get(name).forEach(p -> countsOfLocatedMp3s.put(p, count));
				
			}
			else {
				// mp3 may have been deleted since it was played.  not an error condition
				Logger.finest(this, "could not locate ["+name+"] (count #"+count+")");
			}
		}
		
		Logger.info(this, "located ["+countsOfLocatedMp3s.size()+"] mp3s from play counts ("+locateTimer.stop()+")");
		
		if(!countsOfLocatedMp3s.isEmpty()) {
			
			hub.add(new LocatedMp3Counts(countsOfLocatedMp3s));
		}
	}
	
	private Map<String, List<Path>> locateAllMp3s(String path) {
		
		try {
			return Files.find(Paths.get(path),
				           		Integer.MAX_VALUE,
				           		(filePath, fileAttr) ->  filePath.toString().endsWith(".mp3"))
					 	.collect(groupingBy(p -> removePath(p.toString())));
			
		} catch (IOException e) {
			Logger.error(this, "encountered error while trying to locate mp3s",e);
		}
		
		return Collections.emptyMap();
	}
	
}
