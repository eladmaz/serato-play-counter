package org.ssldev.playcounter.services;

import java.nio.file.Path;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.id3.AbstractID3v2Frame;
import org.jaudiotagger.tag.id3.AbstractID3v2Tag;
import org.jaudiotagger.tag.id3.ID3v22Frame;
import org.jaudiotagger.tag.id3.ID3v22Tag;
import org.jaudiotagger.tag.id3.ID3v23Frame;
import org.jaudiotagger.tag.id3.ID3v23Tag;
import org.jaudiotagger.tag.id3.ID3v24Frame;
import org.jaudiotagger.tag.id3.ID3v24Tag;
import org.jaudiotagger.tag.id3.framebody.FrameBodyTXXX;
import org.ssldev.core.mgmt.EventHub;
import org.ssldev.core.services.Service;
import org.ssldev.core.utils.Logger;
import org.ssldev.core.utils.StopWatch;
import org.ssldev.playcounter.messages.LocatedMp3Counts;
import org.ssldev.playcounter.messages.PlayCountTagsWritingFinished;

/**
 * writes play counts into an mp3 file ID3 tag.
 * </p>
 * NOTE: this is a destructive operation and files should be backed up prior to execution!
 * </p>
 * Consumes: 
 * <ul>
 * <li>{@link LocatedMp3Counts}</li>
 * </ul>
 * Produces:
 * <ul>
 * <li>{@link PlayCountTagsWritingFinished}</li>
 * </ul> 
 */
public class Id3TagPlayCountWriteService extends Service {
	
	/**
	 * custom TXXX ID3 field serato uses to tag play count in mp3
	 */
	public static final String SERATO_ID3_PLAYCOUNT_KEY = "SERATO_PLAYCOUNT";

	
	public Id3TagPlayCountWriteService(EventHub hub) {
		super(hub);
	}

//	private static java.util.logging.Logger[] pins;
	
	@Override
	public void init() {
		hub.register(LocatedMp3Counts.class, this::writePlayCountTags);
	}
	
	
	private void writePlayCountTags(LocatedMp3Counts msg) {
		Logger.info(this, "about to write tags for ["+msg.countsByMp3Path.size()+"] files");
		
		StopWatch playcountWriteTimer = new StopWatch().start();
		
		msg.countsByMp3Path.forEach(this::setPlayCount);
		
		Logger.info(this, "finished writing tags for ["+msg.countsByMp3Path.size()+"] files in ["+playcountWriteTimer.stop()+"] time");
		
		hub.add(new PlayCountTagsWritingFinished());
	}
	
	// used to append leading and trailing whitespace to every play count (needed for serato to read value correctly)
	private static StringBuilder countBuilder = new StringBuilder();
	
	private void setPlayCount(Path p, long count) {

		countBuilder.setLength(0);
		
		// appending whitespace is needed for serato to read value correctly
		countBuilder.append(" ").append(String.valueOf(count)).append(" ");
		String refcount = countBuilder.toString();
		
		Logger.info(this, "... writing count ["+refcount+"] tag for ["+p+"]");
						
		try {
			
			AudioFile f = AudioFileIO.read(p.toFile());
			
			setPlayCountTag( f, SERATO_ID3_PLAYCOUNT_KEY, refcount); 
			
		} catch (Throwable e) {
			Logger.error(this, "encountered error while attempting to set play counts for ["+p+"]",e);
		}
		
	}
	
	/**
	 * This will write a custom ID3 tag (TXXX).
	 * This works only with MP3 files (Flac with ID3-Tag not tested).
	 * </p>
	 * @param description 
	 * 			The description of the custom tag i.e. "catalognr"
	 * 			There can only be one custom TXXX tag with that description in one MP3 file
	 * @param text 
	 * 			The actual text to be written into the new tag field
	 * @return True if the tag has been properly written, false otherwise
	 */

	private static boolean setPlayCountTag(AudioFile audioFile, String description, String text){
	    FrameBodyTXXX txxxBody = new FrameBodyTXXX();
	    txxxBody.setDescription(description);
	    txxxBody.setText(text);

	    // Get the tag from the audio file
	    Tag tag = audioFile.getTagOrCreateAndSetDefault();
	    String frameId = "";

	    AbstractID3v2Frame frame = null;
	    AbstractID3v2Tag v2tag = null;
	    if(tag instanceof AbstractID3v2Tag) {
	    	v2tag = (AbstractID3v2Tag) tag;
	    }
	    if(tag instanceof ID3v23Tag){
	    	frameId = "TXXX";
	        frame = new ID3v23Frame("TXXX");
	    }
	    else if(tag instanceof ID3v24Tag){
	    	frameId = "TXXX";
	        frame = new ID3v24Frame("TXXX");
	    }
	    else if(tag instanceof ID3v22Tag) {
	    	frameId = "TXX";
	    	frame = new ID3v22Frame("TXX");
	    }
	    else {
	    	Logger.warn(Id3TagPlayCountWriteService.class, "encountered unsupported ID3 tag ["+
	    				tag.getClass()+"]. file ["+audioFile.getFile().getAbsolutePath()+"] will not be tagged");
	    	return false;
	    }

	    
	    frame.setBody(txxxBody);

	    try {
	    	
	    	if(v2tag == null) {
	    		Logger.warn(Id3TagPlayCountWriteService.class, "skip setting count for non-id3v2 tag for file["+audioFile.getFile().getAbsolutePath()+"]");
	    		return false;
	    	}
	    	else {
	    		tag.deleteField(frameId);
//	    		v2tag.addFrame(frame); works
	    		tag.addField(frame);
//	    		tag.setField(frame); // TODO add or set?
	    	}
	    	
		} catch (Throwable e1) {
			Logger.error(Id3TagPlayCountWriteService.class, "encountered error setting TXXX ID3 tag for file ["+audioFile.getFile().getAbsolutePath()+"]",e1);
			return false;
		}
	    
	    try {
	        audioFile.commit();
	    } catch (CannotWriteException e) {
//	        Logger.error(Id3TagPlayCountWriteService.class, "encountered error writing ID3 tag to file ["+audioFile.getFile().getAbsolutePath()+"]",e);
	        return false;
	    }
	    return true;
	}
}
