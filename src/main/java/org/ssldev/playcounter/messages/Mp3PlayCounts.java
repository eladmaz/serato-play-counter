package org.ssldev.playcounter.messages;

import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.requireNonNull;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.ssldev.core.messages.Message;

/**
 * calculated number of times mp3s were played 
 */
public class Mp3PlayCounts extends Message {

	/**
	 * map of the name of the mp3 and the number of times they were played
	 */
	public final Map<String, AtomicLong> playCountsPerMp3Name;
	

	public Mp3PlayCounts(Map<String, AtomicLong> playCountsPerAbsolutePath) {
		
		this.playCountsPerMp3Name = unmodifiableMap(requireNonNull(playCountsPerAbsolutePath));
	}

}
