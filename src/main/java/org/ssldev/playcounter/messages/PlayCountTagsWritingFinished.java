package org.ssldev.playcounter.messages;

import org.ssldev.core.messages.Message;

/**
 * signifies all (or none) tracks ID3 info have been updated with play counts 
 */
public class PlayCountTagsWritingFinished extends Message {

}
