package org.ssldev.playcounter.messages;

import static java.util.Objects.requireNonNull;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.ssldev.core.messages.Message;

/**
 * contains locations of mp3 files in a given directory
 */
public class Mp3Locations extends Message {
	
	/**
	 * top level parent dir containing mp3 files 
	 */
	public final String parentDir;
	/**
	 * locations of mp3 file (same file may be found in multiple locations; assumes same file name means same file)
	 */
	public final Map<String,List<String>> mp3LocationsByFileName;
	
	public Mp3Locations(String parentDir, Map<String,List<String>> mp3LocationsByFileName) {
		this.parentDir =  requireNonNull(parentDir);
		this.mp3LocationsByFileName = Collections.unmodifiableMap(requireNonNull(mp3LocationsByFileName));
	}

}
